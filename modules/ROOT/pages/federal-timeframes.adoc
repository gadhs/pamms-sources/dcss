= Appendix A Federal Timeframes
:policy-number: Appendix A
:policy-title: Federal Timeframes

DCSS strives to always exceed these minimum standards to provide the best possible services for our constituents.

[#case-closure]
== Case Closure – 45 CFR § 303.11

Send CP notice 60 calendar days in advance, if required. After 60 calendar days, close case if appropriate.

[#support-payments]
== Collection and Disbursement of Support Payments – 45 CFR § 302.32

Within 2 business days of receipt by FSR, disburse payment to family or other state.

[#continuing-services-to-tanf-customers]
== Continuing Services to TANF Customers – 45 CFR § 302.33

Within 5 working days of closed TANF, Foster Care or Parent Caretaker Medicaid case offer full services to custodian.

[#enforcement]
== Enforcement – 45 CFR § 303.6

Within 30 calendar days of delinquency, take enforcement action. Income Withholding Order (IWO) required if employed.

[#establish-case-and-maintain-records]
== Establish Cases and Maintain Records – 45 CFR § 302.2

Provide Application for IV-D services:

* Same day to in-person request.
* Within 5 working days of receiving a written or telephone request.

Open a case and establish a case record:

* Within 20 calendar days of receipt of referral, or
* Within 20 calendar days of filing an Application for Services under 45 CFR § 302.33.

[#establish-support-obligations]
== Establish Support Obligations - 45 CFR § 303.4

Within 90 calendar days of locating the alleged father/NCP, regardless of whether paternity is established, establish an order for support or complete service of process.

Within 20 calendar days of locating alleged father/NCP in another state, refer to other state.

Within 5 working days from date locate deemed necessary, refer to locate.

[#locate-noncustodial-parents]
== Locate Noncustodial Parents – 45 CFR § 303.3

Access case through all appropriate locate resources:

* Within 75 calendar days of determining locate is needed.

[#nmsn]
== National Medical Support Notice (NMSN) – 45 CFR § 303.32

Within 2 business days of an employment match, mail NMSN to employer.

[#review-and-adjustment-of-support-orders]
== Review and Adjustment of Support Orders – 45 CFR § 303.8

Within 180 days of written request or date of locating NCP, review must be complete.

[#intergovernmental-cases]
== Services in Intergovernmental Cases – 45 CFR § 303.7

[#central-registry-responsibilities]
=== Central Registry Responsibilities

Within 10 working days of receipt of an Intergovernmental case:

* Ensure that the documentation submitted with the case has been reviewed to determine completeness.
* Forward the case for necessary action either to the central State Parent Locator Service for location services or to the appropriate agency for processing.
* Acknowledge receipt of the case and request any missing documentation; and  	Inform the initiating agency where the case was sent for action.

[#responding-jurisdiction]
=== Responding Jurisdiction - Central Registry and Local Office Responsibilities

Within 5 working days, respond to inquiries from other jurisdictions for a case status review.

Within 10 working days of locating NCP in a different jurisdiction, forward case to that jurisdiction.

Within 10 working days, notify initiating jurisdiction of receipt of new information.

Within 30 working days of receiving a request, provide any order and payment record information requested by a State IV-D agency for a controlling order determination and reconciliation of arrearages, or notify the State IV-D agency when the information will be provided.

[#initiating-jurisdiction]
=== Initiating Jurisdiction – IV-D Agency Responsibilities

Within 10 working days, notify other jurisdiction of any new information.

Within 10 working days, notify other jurisdiction that case has closed and provide basis for the closure.

Within 20 calendar days, act after receiving a request for review/adjustment.

Within 30 calendar days, provide any information to other jurisdiction upon request.

Annually, notify responding jurisdiction of interest charges, if any, owed on overdue support under an initiating jurisdiction order being enforced the responding jurisdiction.
